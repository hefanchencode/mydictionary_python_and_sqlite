import PySimpleGUI as sg
import os
from datetime import date
#from nltk.corpus import wordnet as wn # dictionary
import sqlite3
from sqlite3 import Error

def AddToRecord(Word):
	#Find Today's date
	TodayDate = str(date.today())
	#Add the input to today's log
	logFile = TodayDate+'.txt';
	os.system('touch user_data/'+logFile)
	file_output = open('user_data/'+logFile,'a')
	file_output.write(Word + '\n')
	file_output.close()
	

# def LookIntoDictionary(Word):
# 	WordList = wn.synsets(Word)
# 	# print(wn.synset('dog.n.1').definition())
# 	#print(wn.synset('dog.n.1').examples())
# 	CountDef = 0;
# 	output = '';
# 	for w in WordList:
# 		if Word in w.name():
# 			CountDef += 1;
# 			output += (str(CountDef)+'. '+Word+' '+w.pos()+'.\n')
# 			output += ('DEFINITION:  '+ w.definition() + '\n')
# 			Egs = wn.synset(w.name()).examples();
# 			
# 			if (len(Egs) > 0):
# 				CountEgs = 96;
# 				output += ('EXAMPLES: \n')
# 				for eg in Egs:
# 					CountEgs += 1;
# 					output += (chr(CountEgs) + '. ' +eg)
# 				output += '\n'
# 		output += '\n'
# 	return output

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn
def CheckExistence(Word,c):
	t = (Word,)
	c.execute('SELECT * From words WHERE lemma = ?',t)
	Exist = c.fetchall();
	#print(len(Exist))
	if (len(Exist) == 0):
		c.execute('SELECT lemma From morphology WHERE morph = ?',t)
		Morphy = c.fetchall();
		if (len(Morphy) > 0):
			Word = Morphy[0][0];
		else:
			return('')
	return str(Word)
	
def LookIntoDictionary_DB31(Word,c):
	t = (Word,)
	output = Word+'\n';		
	c.execute('SELECT pos,definition,synsetid From wordsXsensesXsynsets where lemma = ? ORDER BY pos', t)
	Defs = c.fetchall();
	print(len(Defs))
	CountDef = 0;
	for row in Defs:
		CountDef += 1;
		output += (str(CountDef)+'. '+row[0]+'.\n')
		output += ('DEFINITION:  '+ row[1] + '\n')

		#print(row[0],row[1])
		#print(str(row[2]))
		c.execute('SELECT sample From samples WHERE synsetid = %d' % row[2] );
		Egs = c.fetchall();
		if (len(Egs) > 0):
			CountEgs = 96;
			output += ('EXAMPLES: \n')
		
			for eg in Egs:
				CountEgs += 1;
				output += (chr(CountEgs) + '. ' +eg[0])
			output += '\n'
		output += '\n'
	return output
 


############################### Start the Main

sg.theme('BluePurple')

layout = [
          [sg.Text('Search',font = 20), sg.Input(key='-IN-',font = 16),sg.Button('Show Definiton', bind_return_key = 1,font = 20)],
          [ ],
          [ sg.MLine(size=(80,15), font = 16, key='-OUTPUT-'+sg.WRITE_ONLY_KEY)],
          [sg.Button('Exit',font = 20)]
         ]

window = sg.Window('My Dictionary v1', layout)
database = "WordNet31_new.db"
conn = create_connection(database)
c = conn.cursor()

Latest_Word = '';
while True:  # Event Loop
    event, values = window.read()
    print(event, values)
    
    if event in  (None, 'Exit'):
        break
        
    WordIn = (values['-IN-'])
    
    if  (event == 'Show Definiton'):
        # Update the "output" text element to be the definiton and examples
        Word = CheckExistence(WordIn,c)
        if (Word == ''):
        	window['-OUTPUT-'+sg.WRITE_ONLY_KEY].update( 'Can\'t find the definition\n')
        	continue;

        Def = LookIntoDictionary_DB31(Word,c)
        window['-OUTPUT-'+sg.WRITE_ONLY_KEY].update(Def)
        
        if (Word != Latest_Word):
        	AddToRecord(Word)
        	Latest_Word = Word
        print(Word)
        
   # if event == 'Add':
    	
conn.close()
window.close()
